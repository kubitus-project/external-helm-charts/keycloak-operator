##@ Build Dependencies

## Location to install dependencies to
LOCALBIN ?= $(shell pwd)/bin
$(LOCALBIN):
	mkdir -p $(LOCALBIN)

## Tool Binaries
KUSTOMIZE ?= $(LOCALBIN)/kustomize

## Tool Versions
# renovate: datasource=github-releases depName=kubernetes-sigs/kustomize extractVersion=^kustomize/(?<version>.+)$
KUSTOMIZE_VERSION ?= v5.6.0

.PHONY: kustomize
kustomize: $(KUSTOMIZE) ## Download kustomize locally if necessary. If wrong version is installed, it will be removed before downloading.
$(KUSTOMIZE): $(LOCALBIN)
	@if test -x $(LOCALBIN)/kustomize && ! $(LOCALBIN)/kustomize version | grep -q $(KUSTOMIZE_VERSION); then \
		echo "$(LOCALBIN)/kustomize version is not expected $(KUSTOMIZE_VERSION). Removing it before installing."; \
		rm -rf $(LOCALBIN)/kustomize; \
	fi
	test -s $(LOCALBIN)/kustomize || GOBIN=$(LOCALBIN) GO111MODULE=on go install sigs.k8s.io/kustomize/kustomize/v5@$(KUSTOMIZE_VERSION)

# renovate: datasource=github-releases depName=helm/helm
HELM_VERSION ?= v3.17.1
HELM ?= $(LOCALBIN)/helm

.PHONY: helm
helm: $(HELM) ## Download helm locally if necessary. If wrong version is installed, it will be removed before downloading.
$(HELM): $(LOCALBIN)
	@if test -x $(LOCALBIN)/helm && ! $(LOCALBIN)/helm version | grep -q $(HELM_VERSION); then \
		echo "$(LOCALBIN)/helm version is not expected $(HELM_VERSION). Removing it before installing."; \
		rm -rf $(LOCALBIN)/helm; \
	fi
	test -s $(LOCALBIN)/helm || \
	  (cd $(LOCALBIN) && curl --location --remote-name "https://get.helm.sh/helm-$(HELM_VERSION)-linux-amd64.tar.gz" && \
	  tar --verbose --extract --gzip --file="helm-$(HELM_VERSION)-linux-amd64.tar.gz" --strip-components=1 linux-amd64/helm && \
	  rm "helm-$(HELM_VERSION)-linux-amd64.tar.gz" && \
	  chmod +x helm)

# Local additions ...
KUBERNETES_SPLIT_YAML ?= $(LOCALBIN)/kubernetes-split-yaml
# renovate: datasource=github-releases depName=yeahdongcn/kubernetes-split-yaml
KUBERNETES_SPLIT_YAML_VERSION ?= v0.4.0
KUSTOHELMIZE ?= $(LOCALBIN)/kustohelmize
# renovate: datasource=github-releases depName=yeahdongcn/kustohelmize
KUSTOHELMIZE_VERSION ?= v0.4.2

define HELM_CHART_DETAILS

home: https://gitlab.com/kubitus-project/external-helm-charts/keycloak-operator/#keycloak-operator-helm-chart
sources:
- https://gitlab.com/kubitus-project/external-helm-charts/keycloak-operator/
maintainers:
- name: Mathieu Parent
  email: math.parent@gmail.com
icon: https://gitlab.com/uploads/-/system/project/avatar/50848788/keycloak.png
annotations:
  artifacthub.io/category: security
  artifacthub.io/operatorCapabilities: deep insights
endef
export HELM_CHART_DETAILS

.PHONY: kubernetes-split-yaml
kubernetes-split-yaml: $(KUBERNETES_SPLIT_YAML) ## Download kubernetes-split-yaml locally if necessary.
$(KUBERNETES_SPLIT_YAML): $(LOCALBIN)
	test -s $(LOCALBIN)/kubernetes-split-yaml || GOBIN=$(LOCALBIN) go install github.com/yeahdongcn/kubernetes-split-yaml@$(KUBERNETES_SPLIT_YAML_VERSION)

.PHONY: kustohelmize
kustohelmize: $(KUSTOHELMIZE) ## Download kustohelmize locally if necessary.
$(KUSTOHELMIZE): $(LOCALBIN) kubernetes-split-yaml
	test -s $(LOCALBIN)/kustohelmize || GOBIN=$(LOCALBIN) GO111MODULE=on go install github.com/yeahdongcn/kustohelmize@$(KUSTOHELMIZE_VERSION)

# renovate: datasource=github-releases depName=keycloak/keycloak
KEYCLOAK_VERSION ?= 26.1.3

.PHONY: manifests
manifests:
	rm -rf keycloak-k8s-resources
	git clone \
	  --quiet \
      --branch "$(KEYCLOAK_VERSION)" \
      --single-branch \
      --depth 1 \
      https://github.com/keycloak/keycloak-k8s-resources

.PHONY: config/keycloak-operator.yaml
config/keycloak-operator.yaml: kustomize
	cp config/kustomization.yaml keycloak-k8s-resources/kubernetes && \
	  $(KUSTOMIZE) build keycloak-k8s-resources/kubernetes --output config/keycloak-operator.yaml
	  sed --in-place \
	    --expression '/app.quarkus.io\/build-timestamp:/d' \
		--expression '/app.quarkus.io\/quarkus-version:/d' \
		config/keycloak-operator.yaml

.PHONY: helm-chart-create
helm-chart-create: manifests config/keycloak-operator.yaml kustohelmize ## Create an helm chart.
	rm -rf charts/keycloak-operator && mkdir -p charts/keycloak-operator/templates && \
	  cp src/hpa.yaml charts/keycloak-operator/templates/keycloak-operator-hpa.yaml && \
	  $(KUSTOHELMIZE) create charts/keycloak-operator \
	  --from=config/keycloak-operator.yaml \
	  --version=0.0.0 \
	  --app-version=$(KEYCLOAK_VERSION) \
	  --description="Keycloak Operator" && \
	  echo "$$HELM_CHART_DETAILS" >> charts/keycloak-operator/Chart.yaml
	  patch --strip 1 < src/post-kustohelmize.diff

.PHONY: helm-chart-package
helm-chart-package: helm
	$(HELM) package charts/keycloak-operator --version=$(HELM_CHART_VERSION) && \
	  $(HELM) template keycloak-operator-*.tgz
