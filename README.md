# Keycloak Operator Helm chart

This is an Helm chart for [Keycloak Operator](https://www.keycloak.org/guides#operator).

## Usage

```shell
helm repo add kubitus-keycloak-operator https://gitlab.com/api/v4/projects/50848788/packages/helm/stable

helm install kubitus-keycloak-operator/keycloak-operator
```

Available values can be seen [here](values.yaml), or retrieved with:

```shell
helm show values kubitus-keycloak-operator/keycloak-operator
```

## Contributing

- File bugs and feature requests in
  [GitLab issues](https://gitlab.com/kubitus-project/external-helm-charts/keycloak-operator/-/issues).
  Security issues should be marked as **confidential**.
- Propose documentation or code improvements in
  [GitLab merge requests](https://gitlab.com/kubitus-project/external-helm-charts/keycloak-operator/-/merge_requests).

  This repository enforces commit message convention, to check this locally install the
  [commitlint](https://github.com/conventional-changelog/commitlint/#what-is-commitlint)
  hook:

  ```shell
  npm install  @commitlint/{config-conventional,cli}
  echo 'npx commitlint --edit' >> .git/hooks/commit-msg
  chmod +x .git/hooks/commit-msg
  ```
